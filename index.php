<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title>KETO.ph</title>

	<!-- fav icon -->
	<link rel="icon" type="image/gif/png" href="./assets/images/fav-icon.png">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Dosis|Germania+One|Lobster" rel="stylesheet">
	
	<!-- bootstrap CSS and JS dependencies -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	
	<!-- external CSS -->
	<link rel="stylesheet" href="./assets/css/style.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	
</head>
<body>

	<div class="wrapper">
	
		<div class="box1">

			
		</div>

		<div class="box2">
			<div class="message-container">
				<h3>KETO.ph</h3>
			<h4>A support group for Filipinos doing Keto</h4>
		
			<a class="btn bg-light btn-block" href="./assets/php/home.php">Explore </a>

			<a class="btn btn-light btn-block" href="./assets/php/signup.php">Join us now and enjoy cool features of our community.</a>

			</div>
		</div>

		<div class="box3">
			
		</div>		

		<div class="box4">
			
		</div>

	</div>	

</body>
</html>	

