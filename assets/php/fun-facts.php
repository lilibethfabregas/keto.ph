<?php 
	include("./header.php"); 
	include("./nav.php");
?>



<!--content  -->

<h1 class="text-center my-4">Fun Facts </h1>

<div class="container">

	<div class="row col-11 fun-facts">

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff4.png" alt="Card image cap">
				</div>
				
				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff5.png" alt="Card image cap">
				</div>

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff6.png" alt="Card image cap">
				</div>

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff1.png" alt="Card image cap">
				</div>
				
				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff2.png" alt="Card image cap">
				</div>

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff3.png" alt="Card image cap">
				</div>

					<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff7.png" alt="Card image cap">
				</div>
				
				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff8.png" alt="Card image cap">
				</div>

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff9.png" alt="Card image cap">
				</div>


				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff10.png" alt="Card image cap">
				</div>
				
				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff11.png" alt="Card image cap">
				</div>

				<div class="card col-lg-4 col-md-6 col-12 border-0 my-2">
				  <img class="card-img-top img-fluid" src="../images/ff12.png" alt="Card image cap">
				</div>


	</div> <!-- closing of row -->

</div> <!-- closing of container -->



<?php
	include("./footer.php");
?>