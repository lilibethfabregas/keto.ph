<?php include("./header.php");?>

    <div class="container-fluid login-page">
        <div class="col-lg-4 col-md-12 col-12 col-md-offset-4 well login-form py-4">
          <h2 class="text-center">Login</h2>
              <form method="POST" action="checklogin.php">
                 <div class="form-group">
                       <label for="email">Email address</label>
                       <input type="email" class="form-control"  name="email" placeholder="Email" required="">
                  </div>
                  <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required="">
                  </div>
                  <div class="text-center">
                       <button type="submit" class="my-4 btn btn-success btn-block">Login</button>
                 </div>
                 <a href="signup.php" class="my-4">Not a member yet? Signup</a>

                 <?php  
                     if(isset($_SESSION['registered'])) {
                   ?>
                   <div>
                       <p class="text-center">You have successfully registered.</p>
                   </div>
                  <?php 
                    unset($_SESSION['registered']); }
                  ?>
                  <?php
                    if(isset($_SESSION['loginError'])) {
                  ?>
                     <div>
                         <p class="text-center">Invalid Email/Password. Try again.</p>
                     </div>
                 <?php 
                    unset($_SESSION['loginError']); }
                ?>
               </form>
              </div>
             </div>
            </div>   
  </section>
  </body>
</html> 