<?php 
	include("./header.php"); 
	include("./nav.php");
?>



<!--content  -->

<h1 class="text-center my-4">What's new this week? </h1>
<div class="container">

	<div class="row py-4">
	
			<div class="col-lg-1 col-md-2 col-1"></div>


		<div class="col-lg-5 col-md-5 col-12">

			  <h5 class="h4 text-center">Biggest Loser this week</h5>

					<div class="card w-100" >

						<img class="card-img-top img-fluid" src="https://scontent.fmnl17-1.fna.fbcdn.net/v/t1.0-9/57439850_10157181467518839_1641289355228086272_n.jpg?_nc_cat=101&_nc_ht=scontent.fmnl17-1.fna&oh=d5ec014a75abdb0aa02fd1052fffe6ed&oe=5D390CF5" alt="Card image cap">

					</div> <!-- closing of card biggest loser image -->
					
		</div> <!-- closing of div biggest loser image - col-lg-5 -->		
		

		<div class="col-lg-6 text-justify col-md-2 col-12  mr-auto">

			<div class="card w-100 my-4 border-0">
				<div class="card-body biggest-loser">
				
							<h5 class="h4 text-center">Christina Michelle:</h5>				
				  <p class="card-text">"Today the boots got put away and the sandals came out! Spring is on its way! I can now wear a dress and not feel like the "fat" girl anymore...I hate my left knee....but you know what we all have a part that we hate and that we try to cover...As hard as I have worked I am no longer scared to share or be who I am....
				  </p>	
				  <p>Starting Weight: 299.9</p>
				  <p>Current Weight: 170-180....somewhere in there lol</p>
				  <p>Goal Weight: 145</p>
				  <p>Date Started: 3-20-17</p>
				  <p>Guys I have had a hard time mentally this year...but its not a bad thing, this is a mental, physical and so many more emotions roller-coaster! Proving to do this for our-self is what this journey is about. Finding what works for you is what matters, what works for me might not work for you...
					</p>
					<p>
						
					But damnit! I have worked hard and proud of these results...yes I am NOT where I want to be but if it takes me another two years I will get there....regardless of the outcome I will do..
					</p>
					<p>I am doing this for ME!!!"</p>  

				  
				</div> <!-- closing of card - biggest-loser text content -->
				
			</div><!--  closing of card - biggest loser text content -->
 		</div> <!-- closing of div col-6 - biggest-loser text content -->
		

	</div><!--  closing row -->

	<div class="row">
		<div class="col-lg-1 col-md-1 col-12">
			
		</div>

		<div class="col-lg-5 col-md-5 col-12">
			
		</div>

		<div class="col-lg-5 col-md-5 col-12">
			
		</div>

	</div>


</div> <!-- closing of container -->



<?php
	include("./footer.php");
?>