<?php include("./header.php");?>
  
    <div class="container-fluid signup-page">
        <div class="col-lg-4 col-md-12 col-12 col-md-offset-4 well signup-form py-2">
          <h2 class="text-center">Register now</h2>
              <form method="POST" action="checksignup.php">
                   <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" id="firstname" name="first_name" placeholder="First Name" required="">
                    </div>

                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" id="lastname" name="last_name" placeholder="Last Name" required="">
                    </div>

                     <div class="form-group">
                        <label for="bday">Birthday</label>
                        <input type="date" class="form-control" id="bday" name="bday" placeholder="Birthday" required="">
                    </div>


                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="">
                    </div>

                    <div class="form-group">
                        <label for="mobile">Mobile Number</label>
                        <input type="mobile" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required="">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                    </div>

                     <div class="text-center">
                        <button type="submit" class="btn btn-success btn-block">Signup</button>
                    </div>
                    <a href="login.php">Already a member? Login</a>

               <?php  
                  if(isset($_SESSION['registerError'])) {
                ?>
                   <div>
                       <p class="text-center">Email already taken. Choose another email.</p>
                   </div>
               <?php 
                  unset($_SESSION['registerError']); }
                ?>

              </form>
           </div>
        </div>
    </div>   
  </body>
</html> 