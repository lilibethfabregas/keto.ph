<?php 
	include("./header.php"); 
	include("./nav.php");
?>


<!-- landing banner -->
		<div class="card border-0">
			<img src="../images/about-img.jpg" class="card-img-top" alt="...">
		</div>

<!--content  -->

		<div class="container">
			<div class="col-2 social-btns">
					<div class="list-group position-fixed animated pulse infinite">
						<a href="#" class="list-group-item border-0 list-group-item-action"> <span><i class="btn-facebook fab fa-facebook-f"></i></span> </a>
						<a href="#" class="list-group-item border-0 list-group-item-action"> <span><i class="btn-youtube fab fa-youtube"></i></span> </a>
						<a href="#" class="list-group-item border-0 list-group-item-action">	<span><i class="btn-twitter fab fa-twitter"></i></span> </a>
						<a href="#" class="list-group-item border-0 list-group-item-action"> <span><i class="btn-instagram fab fa-instagram"></i></span></a>
					
					</div><!-- closing of social media container -->
			</div><!--  closing of col-2 -->


			<div class="col-md-8 text-justify col-12 about-content">

					<div class="card-body card-about col-12 mx-auto">

							<h4 class="card-title">What is KETO.ph?</h4>
							<hr>

								<p class="card-text">KETO.ph is a support group for Filipinos doing Ketogenic diet. </p>
								<p>Ketogenic Diet Support group, we share success stories in fitness and wellness. We share recipes and ingredients that support Keto diet. We encourage love and friendship among our members. Welcome to our growing Keto Family! </p>
							
							<h4 class="card-title">What is ketogenic diet?</h4>
							<hr>
								<p class="card-text">The ketogenic diet is a high-fat, adequate-protein, low-carbohydrate diet that in medicine is used primarily to treat difficult-to-control (refractory) epilepsy in children. The diet forces the body to burn fats rather than carbohydrates. Normally, the carbohydrates contained in food are converted into glucose, which is then transported around the body and is particularly important in fueling brain function. However, if little carbohydrate remains in the diet, the liver converts fat into fatty acids and ketone bodies. The ketone bodies pass into the brain and replace glucose as an energy source. An elevated level of ketone bodies in the blood, a state known as ketosis.
								</p>

							<div class="card video-keto col-lg-6 col-12 mx-auto border-0 my-4">
								<iframe w-100 height="315" src="https://www.youtube.com/embed/s5w-PyeJOrs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>

							<p class="card-text">Ketosis is a normal metabolic process, something your body does to keep working. When it doesn't have enough carbohydrates from food for your cells to burn for energy, it burns fat instead. As part of this process, it makes ketones. If you're healthy and eating a balanced diet, your body controls how much fat it burns, and you don't normally make or use ketones. But when you cut way back on your calories or carbs, your body will switch to ketosis for energy. It can also happen after exercising for a long time and during pregnancy. For people with uncontrolled diabetes, ketosis is a sign of not using enough insulin.
							</p>

							<p class="card-text">Ketosis is a popular weight loss strategy. Low-carb eating plans include the first part of the Atkins diet and the Paleo diet, which stress proteins for fueling your body. In addition to helping you burn fat, ketosis can make you feel less hungry. It also helps you maintain muscle.
							</p> 
							
							<p class="card-text">For healthy people who don't have diabetes and aren't pregnant, ketosis usually kicks in after 3 or 4 days of eating less than 50 grams of carbohydrates per day. That's about 3 slices of bread, a cup of low-fat fruit yogurt, or two small bananas. You can start ketosis by fasting, too.
							</p> 
							
							<p class="card-text">Doctors may put children who have epilepsy on a ketogenic diet, a special high-fat, very low-carb and protein plan, because it might help prevent seizures. Adults with epilepsy sometimes eat modified Atkins diets.
							</p> 
							
							<p class="card-text">Some research suggests that ketogenic diets might help lower your risk of heart disease. Other studies show specific very-low-carb diets help people with metabolic syndrome, insulin resistance, and type 2 diabetes. Researchers are also studying the effects of these diets on acne, cancer, polycystic ovary syndrome (PCOS), and nervous system diseases like Alzheimer's, Parkinson's, and Lou Gehrig's disease.
							</p> 

					<hr>
				<h4>Benefits of Ketogenic Diet	</h4>

				<h5 >Lose Weight	</h5>
			
				<p>Most people start eating fewer carbs to lose weight. Studies has shown that low-carb diets are often more effective than other diets. Low carb makes it easier to lose weight without hunger, and without having to count calories. According to new science, a low-carb diet can even result in burning more calories than other diets.</p>

				<h5>Reverse type 2 diabetes	</h5>
			
				<p>Low-carb diets can help reduce or even normalize blood sugar, and thus potentially reverse type 2 diabetes.</p>

				<h5>A grateful gut</h5>
			
				<p>Low carb might help settle a grumpy gut, often reducing symptoms of irritable bowel syndrome such a bloating, gas, diarrhea, cramps and pain. Indigestion, reflux and other digestive issues can sometimes improve, too. For some, this is the best part of going low carb and happens usually within the first few days, or first week, of starting the diet.</p>

				<h5>Reduce sugar cravings</h5>
			
				<p>Are you struggling to stay away from sweet foods, even though you try to eat them in “moderation”? Many people do. A low-carb diet can often reduce and sometimes even eliminate cravings for sweets </p>
			
				<h5>Bonus Benefits: Normalize, cure and regulate: </h5>
			
				<p>Blood pressure, acne, physical endurance, epilepsy, heartburn, PCOS, migraine, cancer.</p>
	
				<hr>

				<h4>Testimonials	</h4>
				
				<p>Well, they say pictures are worth a thousand words. So instead of words, we'll show you photos first.</p>
	

		<div class="card-testi">
		<input type="checkbox" name="">
		<div class="toggle">+</div>

		<div class="imgBx">
			<img src="./assets/images/Untitled-3.png" alt="">
		</div>
		<div class="details">
			<h2>Someone Famous</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur doloribus dolorum quidem unde corporis ullam quia, perferendis et mollitia tenetur possimus iure, rerum amet? Quasi voluptas quidem, eum debitis maxime!</p>
		</div>

		<div class="imgBx">
			<img src="./assets/images/Untitled-3.png" alt="">
		</div>
		<div class="details">
			<h2>Someone Famous</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur doloribus dolorum quidem unde corporis ullam quia, perferendis et mollitia tenetur possimus iure, rerum amet? Quasi voluptas quidem, eum debitis maxime!</p>
		</div>
	</div>
	

		
		</div> <!-- closing of card body col-12 -->

	</div> <!-- closing of col-10 for about-contents -->

</div> <!-- closing of container -->
	

		






<?php
	include("./footer.php");
?>