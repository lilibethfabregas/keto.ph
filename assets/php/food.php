<?php 
	include("./header.php"); 
	include("./nav.php");
?>

		<div class="card border-0">
			<img src="../images/food-img.jpg" class="card-img-top" alt="...">
		</div>
		
		<div class="container ">
			<div class="col-2 food-side">
				
			</div> <!-- closing of col 2 -->
			
			<div class="col-md-8 text-justify col-12 food-content ">

				<div class="card-body card-about">
					<h4 class="card-title">What to eat during Keto diet?</h4>
					<p class="card-text">The numbers above are grams of digestible carbs per 100 grams (3.5 ounces). Fiber is not counted, you can eat all the fiber you want. All foods above are below 5% carbs. Sticking to these foods will make it relatively easy to stay on a strict low-carb diet, with less than 20 grams of carbs per day.</p>

						<div class="col-md-10 col-12">
							<div class="accordion " id="whatToEat">
							  <div class="card">
							  
							    <div class="card-header" id="headingOne">
							      <h2 class="mb-0">
							        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							          Meat 
							        </button>
							      </h2>
							    </div> <!-- end of heading one -->

							    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#whatToEat">
							      <div class="card-body shopping-guide-meat">
							        Feel free to eat the fat on the meat as well as the skin on the chicken. You may want to choose organic or grass-fed meats.

							      	<ul> Any type of:
							      		<li>Beef <img src="https://cdn.britannica.com/s:300x300/68/143268-004-6D674B58.jpg" alt=""></li>
							      		<li>Pork <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Schweinebauch-2.jpg/1200px-Schweinebauch-2.jpg" alt=""></li>
							      		<li>Lamb <img src="http://allnaturalmeats.ca/wp-content/uploads/2016/10/lamb-48717395_xxl-19.jpg" alt=""></li>
							      		<li>Poultry + eggs<img src="https://farmerstoyou.com/images/products/whole_medium_chicken_4-4.5_lbs._109.jpg" alt=""></li>
							      	</ul>
							      </div> <!-- end of div meat -->

							    </div> <!-- end of collapse one -->
							  </div> <!-- end of card one -->

							  <div class="card">
							    <div class="card-header" id="headingTwo">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Fish and seafood
							        </button>
							      </h2>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#whatToEat">
							      <div class="card-body">
							         All kinds: Fatty fish such as salmon, mackerel, sardines or herring are great, and might even have health benefits due to high amounts of omega-3 fatty acids.
							        </div> <!-- end of div seafood -->

							    </div> <!-- end of collapse two -->
							  </div> <!-- end of card two -->

							<div class="card">
							    <div class="card-header" id="headingFour">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							  	    Natural fats and high-fat sauces
							        </button>
							      </h2>
							    </div>
							    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
								   </div> <!-- end of div fats -->

							    </div> <!-- end of collapse four -->
							  </div> <!-- end of card four -->
							
							<div class="card">
							    <div class="card-header" id="headingFive">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							  	    Vegetables that grow above ground
							        </button>
							      </h2>
							    </div>
							    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
							      </div> <!-- end of div veggies -->

							    </div> <!-- end of collapse five -->
							  </div> <!-- end of card five -->

							  <div class="card">
							    <div class="card-header" id="headingSix">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
							  	    Dairy products
							        </button>
							      </h2>
							    </div>
							    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
							      </div> <!-- end of div dairy -->

							    </div> <!-- end of collapse six -->
							  </div> <!-- end of card six -->


							  <div class="card">
							    <div class="card-header" id="headingSeven">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
							  	    Nuts
							        </button>
							      </h2>
							    </div>
							    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
								    </div> <!-- end of div nuts -->

							    </div> <!-- end of collapse seven -->
							  </div> <!-- end of card seven -->


							  <div class="card">
							    <div class="card-header" id="headingEight">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
							  	    Fruits
							        </button>
							      </h2>
							    </div>
							    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
							    	</div> <!-- end of div fruits -->

							    </div> <!-- end of collapse eight -->
							  </div> <!-- end of card eight -->

							
							  <div class="card">
							    <div class="card-header" id="headingNine">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
							  	    Drinks
							        </button>
							      </h2>
							    </div>
							    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
							     </div> <!-- end of div drinks -->

							    </div> <!-- end of collapse nine -->
							  </div> <!-- end of card nine -->
							
							<div class="card">
							    <div class="card-header" id="headingTen">
							      <h2 class="mb-0">
							        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
							  	    Food to Avoid
							        </button>
							      </h2>
							    </div>
							    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#whatToEat">
							      <div class="card-body">
							        Using butter and cream for cooking can make your low-carb foods taste better and can make you feel more satisfied. Try a Béarnaise or Hollandaise sauce. If purchased pre-made, check the ingredients for starches and vegetable oils. Better yet, make it yourself. Coconut fat or olive oil are also good options.
							      </div> <!-- end of div avoid -->

							    </div> <!-- end of collapse ten -->
							  </div> <!-- end of card ten -->
							
						

						</div> <!-- closing of accordion -->
					</div> <!-- closing of col-10 holder of accordion -->
				</div> <!-- closing of card body -->

		<h4 class="card-title">Shopping Guide || Philippine Version</h4>				
			<h5>Stores</h5>


		<h4 class="card-title">Recipes || Philippine Version</h4>				

		<h4 class="card-title">Food Deliveries</h4>	
		

		<hr>		
		<h4 class="card-title">Is this keto?</h4>
		
		<form action="">
			<input type="text" placeholder="Type food here and check if its keto." d-block>
			<input type="button" value="Check">
		</form>	
		
	

	</div> <!-- closing of col-10 food content -->
</div> <!-- closing of container -->
				








<?php
	include("./footer.php");
?>